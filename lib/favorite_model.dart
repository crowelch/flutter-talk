import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

const String TABLE_FAVORITE = 'favorite';
const String COLUMN_ID = '_id';
const String COLUMN_NAME = 'name';
const String COLUMN_SAVED = 'saved';

class FancyName {
  int id;
  String name;
  bool saved;

  FancyName();

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{COLUMN_NAME: name, COLUMN_SAVED: saved ? 1 : 0};

    if (id != null) {
      map[COLUMN_ID] = id;
    }

    return map;
  }

  FancyName.fromMap(Map<String, dynamic> map) {
    id = map[COLUMN_ID];
    name = map[COLUMN_NAME];
    saved = map[COLUMN_SAVED] == 1;
  }
}

class FavoriteProvider {
  Database db;

  Future open() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'flutter.db');

    db = await openDatabase(path, version: 2,
        onCreate: (Database db, int version) async {
      await db.execute(
          'CREATE table $TABLE_FAVORITE ($COLUMN_ID integer primary key autoincrement, $COLUMN_NAME text not null, $COLUMN_SAVED integer)');
    });
  }

  Future<FancyName> insert(String name) async {
    FancyName favoriteModel = FancyName();
    favoriteModel.name = name;
    favoriteModel.id = await db.insert(TABLE_FAVORITE, favoriteModel.toMap());
    return favoriteModel;
  }

  Future<int> update(FancyName favorite) async {
    return await db.update(TABLE_FAVORITE, favorite.toMap(),
        where: '$COLUMN_ID = ?', whereArgs: [favorite.id]);
  }

  Future<FancyName> getFavorite(String name) async {
    List<Map> favorites = await db.query(TABLE_FAVORITE,
        columns: [COLUMN_ID, COLUMN_NAME],
        where: '$COLUMN_NAME = ?',
        whereArgs: [name]);
    if (favorites.length > 0) {
      return FancyName.fromMap(favorites.first);
    } else {
      return null;
    }
  }

  Future<List<FancyName>> getFavorites() async {
    List<Map> favorites = await db.query(TABLE_FAVORITE,
        columns: [COLUMN_ID, COLUMN_NAME],
        where: '$COLUMN_SAVED = ?',
        whereArgs: [1]);
    if (favorites.length > 0) {
      return favorites.map((favorite) => FancyName.fromMap(favorite));
    } else {
      return null;
    }
  }

  Future<int> delete(String name) async {
    return await db
        .delete(TABLE_FAVORITE, where: '$COLUMN_ID = ?', whereArgs: [name]);
  }

  Future close() async => db.close();
}

class FavoriteDb {
  Future<FancyName> insert(String name) async {
    FavoriteProvider favoriteProvider = FavoriteProvider();
    favoriteProvider.open();
    FancyName favoriteModel = await favoriteProvider.insert(name);
    favoriteProvider.close();
    return favoriteModel;
  }

  Future<FancyName> toggleFavorite(String name) async {
    FavoriteProvider favoriteProvider = FavoriteProvider();
    favoriteProvider.open();
    FancyName favoriteModel = await favoriteProvider.getFavorite(name);
    favoriteModel.saved = !favoriteModel.saved;
    favoriteProvider.update(favoriteModel);
    favoriteProvider.close();
    return favoriteModel;
  }

  Future<int> delete(String name) async {
    FavoriteProvider favoriteProvider = FavoriteProvider();
    favoriteProvider.open();
    int result = await favoriteProvider.delete(name);
    favoriteProvider.close();
    return result;
  }

  Future<bool> contains(String name) async {
    FavoriteProvider favoriteProvider = FavoriteProvider();
    favoriteProvider.open();

    FancyName favoriteModel = await favoriteProvider.getFavorite(name);
    favoriteProvider.close();
    return favoriteModel != null;
  }
}
